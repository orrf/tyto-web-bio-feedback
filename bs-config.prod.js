const {createProxyMiddleware} = require('http-proxy-middleware');
module.exports = {
    port: 9091,
    files: ["./dist/src/**/*.{html,htm,css,js}","./dist/index.html"],
    server: {
        baseDir: ["./dist"],
        middleware: [createProxyMiddleware('/socket.io', {
            target: "http://localhost:3000",
            changeOrigin: true
        })]
    },
    serveStatic: ["./dist", "./node_modules"]

}