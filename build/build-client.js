const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const del = require('del');




async function build() {
    await del(path.resolve(__dirname, '../dist'));
    fs.mkdirSync(path.resolve(__dirname, '../dist'), {
        recursive: true
    });

    glob(`${path.resolve(__dirname, '../src/**/*.{js,html,css,gif}')}`, (err, matches) => {
        console.log({
            matches
        });
        const local = process.argv.includes('-l');
        matches.forEach(fileName => {
            let fileContent = fs.readFileSync(fileName);
            let res;
            
            if(fileContent.toString().includes('<script src="/socket.io/socket.io.js"></script>') && !local){
                const parts = fileContent.toString().split('<script src="/socket.io/socket.io.js"></script>');
                console.log(parts);
                res = parts[0] + parts[1].split('<script src="../scripts/init-socket.js"></script>')[1];
            }else{
            res = fileName.includes('.gif') ? fileContent : fileContent.toString();
            }
            fs.outputFileSync(path.resolve(__dirname, `../dist/src/${fileName.substring(__dirname.length - 1)}`), res, {
                flag: 'w'
            });
        });
    });
    const indexFile = fs.readFileSync(path.resolve(__dirname, '../index.html'));
    fs.outputFileSync(path.resolve(__dirname, `../dist/index.html`), indexFile);
}

build();