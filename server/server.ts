import { Device } from "./device";
const path = require('path');

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const device = new Device();
device.onData((data) => {
    io.emit('spo2data',data);
})

app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname,'../../index.html'));
});
app.get('/socket.io/socket.io.js', (req, res) => {
    console.log('ippppppppppo')
});

io.on('connection', (socket) => {
    console.log('a user connected');
});

server.listen(3000, () => {
    console.log('listening on *:3000');
});


device.init();