import { HID, devices as hidDevices } from 'node-hid';
import { log } from '../core/utils.js';
const VENDOR_ID = 0x28e9;
const PRODUCT_ID = 0x028a;
const mask7f = 0x7f;


let spo2Device;


interface SpO2Data {
    signalStrength: string
    lowOxSaturation: string
    pulseIndication: string
    barGraph: string
    pulseWave: string
    searchState: string
    fingerStatus: string
    pulseStatus: string
    spo2: string
    spo2Result: string
    pulseRate: string
    pulseRateResult: string
}

export class Device {
    callbacks: any[] = [];
    constructor() {

    }
    onData(callBack: (data) => void) {
        this.callbacks.push(callBack)
    }

    pubData(data: any) {
        this.callbacks.forEach(callback => callback(data));
    }
    init() {
        const [spo2DeviceInfo] = hidDevices().filter(device => device.productId === PRODUCT_ID && device.vendorId === VENDOR_ID);
        if (spo2DeviceInfo) {
            spo2Device = new HID(spo2DeviceInfo.path);

            spo2Device.on('data', (data) => {
                this.parseRealTimePacket(data);
            });
            spo2Device.on('error', (error) => {
                console.log({
                    error
                })
            })
           
            this.sendCommand(this.addCheckSum([0x80]));

            this.sendCommand(this.addCheckSum([0x9b, 0x01]));
            log(`Set real time data`);

            this.sendCommand(this.addCheckSum([0x9b, 0x00]));
            log(`Set wave data`);

            setInterval(() => {
                this.sendCommand(this.addCheckSum([0x9a]));
                log(`Interval keep alive`);
            }, 5000);
        }
    }
    sendCommand(command) {
        spo2Device.write([0x00].concat(...command));
    }

    addCheckSum(commandsArr: any[]) {
        const sum = commandsArr.reduce((p, c) => {
            return p + c;
        }, 0x00);
        return [...commandsArr, sum & 0x7f];
    }
    verifyChecksum(replay: any[], size: number, startIndex: number) {
        let sum = 0;
        let end = startIndex + size;
        for (let i = startIndex; i < end - 1; ++i) {
            sum += replay[i];
        }
        sum = sum & 0x7f;
        const res = sum != replay[end - 1] ? -1 : 0;
        // log(`verifyChecksum:: ${res}`);
        return res;
    }


    parseRealTimePacket(reply: any[]) {
        const reply_id = 0xeb;
        // console.log({reply});
        reply.forEach((d, index) => {
            if (d === reply_id) {
                // data
                if (reply[index + 1] === 0x01) {
                    if (this.verifyChecksum(reply, 8, index) < 0) {
                        console.log('real time data reply - failed on checksum');
                        return;
                    }
                    // console.log('Data verified');
                    let pulseRate = ((reply[index + 2] & 0x02) << 6) | (reply[index + 3] & mask7f);
                    let pulseIndication = (reply[index + 2] & 0x01);
                    let spo2Result = (reply[index + 4] & mask7f);
                    const result = { pulseRate, pulseIndication, spo2Result };
                    this.pubData(result);
                    // wave
                } else if (reply[index + 1] === 0x00) {
                    if (this.verifyChecksum(reply, 6, index) < 0) {
                        console.log('wave reply - failed on checksum');
                        return;
                    }
                    // console.log('Wave verified');
                    let signalStrength = (reply[index + 2] & 0x0f);
                    let searchState = (reply[index + 2] & 0x10) >> 4;
                    let lowOxSaturation = (reply[index + 2] & 0x20) >> 5;
                    let pulseWave = (reply[index + 3] & mask7f);
                    let barGraph = (reply[index + 4] & 0x0f);
                    let fingerStatus = (reply[index + 4] & 0x10) >> 4;
                    let pulseState = (reply[index + 4] & 0x20) >> 5;
                    const res = { signalStrength, searchState, lowOxSaturation, pulseWave, barGraph, fingerStatus, pulseState };
                    this.pubData(res);
                }
            }
        })

    }
}