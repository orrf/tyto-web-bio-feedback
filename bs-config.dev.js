const {createProxyMiddleware} = require('http-proxy-middleware');
module.exports = {
    port: 9090,
    files: ["./src/**/*.{html,htm,css,js}","./index.html"],
    server: {
        baseDir: ["./src"],
        middleware: [createProxyMiddleware('/socket.io', {
            target: "http://localhost:3000",
            changeOrigin: true
        })]
    },
    serveStatic: ["./", "./node_modules"]

}