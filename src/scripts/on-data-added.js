function onDataAdded(data, isString) {
    const event = new CustomEvent('spo2Data', {
        detail: isString ? JSON.parse(atob(data)) : data
    });
    window.dispatchEvent(event);
}

