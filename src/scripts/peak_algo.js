function CreateHeartBeatPeakArr(pulseWaveArr_Inner) {
     
    //console.log("data - "+pulseWaveArr_Inner);
    //   aaaa = pulseWaveArr_Inner.length;
    //console.log("arrat" + aaaa);
    peaksTempArr = [];
    peaksArr = [];

    var params = new Object();
    params.lag = 10;
    params.threshold = 1;
    params.influece = 0.1;
    peaksTempArr = smoothed_z_score(pulseWaveArr_Inner,params);

    for (let i = 0; i < peaksTempArr.length; i++) 
    {
        if (peaksTempArr[i]==1)
            peaksArr.push(i);
    }

    


                /*
                peaksTempArr = detectPeaks(pulseWaveArr, 4, 22);
                
                peaksArr.push(peaksTempArr[0]);
                for (let i = 1; i < peaksTempArr.length; i++) 
                {
                    if (peaksTempArr[i]!=peaksTempArr[i-1]+1)
                        peaksArr.push(peaksTempArr[i]);
                }*/
                console.log(peaksArr);
                return (peaksArr);

}

function detectPeaks_Ido(data, windowWidth, threshold) 
{
    const peaks = [];
    for (let i = 0; i < data.length; i++) {
        const start = Math.max(0, i - windowWidth);
        const end = Math.min(data.length, i + windowWidth);
        let deltaAcc = 0;
        for (let a = start; a < end; a++) {
            deltaAcc += Math.abs(data[a - 1] - data[a]);
        }
        if (deltaAcc > threshold) {
            peaks.push(i);
        }
    }
    return peaks;
}

function smoothed_z_score(y, params) {
    var p = params || {}
    // init cooefficients
    const lag = p.lag || 5
    const threshold = p.threshold || 3.5
    const influence = p.influece || 0.5

    if (y === undefined || y.length < lag + 2) {
        throw ` ## y data array to short(${y.length}) for given lag of ${lag}`
    }
    //console.log(`lag, threshold, influence: ${lag}, ${threshold}, ${influence}`)

    // init variables
    var signals = Array(y.length).fill(0)
    var filteredY = y.slice(0)
    const lead_in = y.slice(0, lag)
    //console.log("1: " + lead_in.toString())

    var avgFilter = []
    avgFilter[lag - 1] = mean(lead_in)
    var stdFilter = []
    stdFilter[lag - 1] = stddev(lead_in)
    //console.log("2: " + stdFilter.toString())

    for (var i = lag; i < y.length; i++) {
        //console.log(`${y[i]}, ${avgFilter[i-1]}, ${threshold}, ${stdFilter[i-1]}`)
        if (Math.abs(y[i] - avgFilter[i - 1]) > (threshold * stdFilter[i - 1])) {
            if (y[i] > avgFilter[i - 1]) {
                signals[i] = +1 // positive signal
            } else {
                signals[i] = 0 // negative signal
            }
            // make influence lower
            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i - 1]
        } else {
            signals[i] = 0 // no signal
            filteredY[i] = y[i]
        }
        // adjust the filters
        const y_lag = filteredY.slice(i - lag, i)
        avgFilter[i] = mean(y_lag)
        stdFilter[i] = stddev(y_lag)
    }
    // console.log(signals)
    var startIndex = -1

    if (getIndexesOfPeaksFromPeakArray(signals).length > Math.floor(y.length / 10)){
        p.threshold += 0.3
        signals = smoothed_z_score(y, p)
    }

    for (i = lag; i < y.length; i++) {
        if (signals[i] == 1){
            if (i > startIndex){
                startIndex = (i + lag*1.5)
                var maxIndex = i + y.slice(i, startIndex).reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);  
                for (var j = i; j < startIndex; j++) {
                    if (j == maxIndex){
                        signals[j] = 1
                    }
                    else{
                        signals[j] = 0
                    }

                }
            }
        }
    }

    return signals
}

function mean(a) {

    return sum(a) / a.length

}