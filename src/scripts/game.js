// center point
var centerX = 0.0, centerY = 0.0;
var radius = 10, rotAngle = -90;
var accelX = 0.0, accelY = 0.0;
var deltaX = 0.0, deltaY = 0.0;
var springing = 0.0005;
var damping = 0.98;
var siganlNoise = 0;
var innerHeartRate = 50;

var innerWindowWidth = 360;
var innerWindowHeight = 640;
//corner nodes
var nodes = 25;
//zero fill arrays
var nodeStartX = [];
nodeStartX[0] = [];
nodeStartX[1] = [];
var nodeStartY = [];
nodeStartY[0] = [];
nodeStartY[1] = [];
var nodeX = [];
nodeX[0] = [];
nodeX[1] = [];
var nodeY = [];
nodeY[0] = [];
nodeY[1] = [];
var angle = [];
angle[0] = [];
angle[1] = [];
var frequency = [];
frequency[0] = [];
frequency[1] = [];
// soft-body dynamics
var organicConstant = 1.0;


var diam = 45;
var change = 2;
var breathingPosY = 100;

function setup() {
//alert (window.innerWidth);
    
  //  font = loadFont('assets/SourceSansPro-Regular.otf');
    createCanvas(innerWindowWidth,innerWindowHeight);

    if (showGIFincenter)
        gif_createImg = createImg("../scripts/tenorSmall.gif");
    

    radius = innerWindowWidth / 3.5;


    //center shape in window
    centerX = innerWindowWidth / 2;
    centerY = innerWindowHeight / 2;
    //initialize arrays to 0
    for (var n = 0; n < 2; n++) {
        for (var i = 0; i < nodes; i++) {
            nodeStartX[n][i] = 0;
            nodeStartY[n][i] = 0;
            nodeY[n][i] = 0;
            nodeY[n][i] = 0;
            angle[n][i] = 0;
        }
    }
    // iniitalize frequencies for corner nodes
    for (var i = 0; i < nodes; i++) {
        randnumber = random(5, 20);
        frequency[0][i] = randnumber;
        frequency[1][i] = randnumber;
    }
    noStroke();
    frameRate(30);
}
function draw() {
 
    
    drawBreathing();    

   // drawBeatGame();
    if (bInLoadingMode)
    {
        //drawLoading();
    }
    else
    {
        drawBeatGame();
    }

    

}

function drawBreathing()
{
    
    background(0);
    // color conditional
    var col;
  
    col = color(109,114,243);
    fill(col);
   
    // diameter change
    diam += change;
    if (diam > 150){
          change = -change;
    } else if (diam < 30){
          change = -change;
    }
   
    ellipse(centerX,breathingPosY,diam, diam);
}

function drawBeatGame()
{
    
    
    //fade background
    fill(0, 100);
    rect(0, 0, width, height);
    fill("#6872f4");
    drawShape(0);
    moveShape(0);
    fill("#000");
    drawShape(1);
    moveShape(1);
    fill(255);

    if (showGIFincenter)
        gif_createImg.position(window.innerWidth/2-30, centerY+40);
}
function drawShape(shapeID) {
    //  calculate node  starting locations
    for (var i = 0; i < nodes; i++) {

        deltaHartRate  = 0;
        deltaHartRate = Math.min((exteranlHeartRate  -  baseHeartRate)*3.5,70);
        //console.log(deltaHartRate);

        nodeStartX[shapeID][i] = centerX + cos(radians(rotAngle)) * (radius - (shapeID * 15)+ deltaHartRate);
        nodeStartY[shapeID][i] = centerY + sin(radians(rotAngle)) * (radius - (shapeID * 15)+ deltaHartRate);
        rotAngle += 360.0 / nodes;
    }
    // draw polygon
    curveTightness(organicConstant);
    beginShape();
    for (var i = 0; i < nodes; i++) {
        curveVertex(nodeX[shapeID][i], nodeY[shapeID][i]);
    }
    for (var i = 0; i < nodes - 1; i++) {
        curveVertex(nodeX[shapeID][i], nodeY[shapeID][i]);
    }
    endShape(CLOSE);

   

    
}

var ind=0;

function moveShape(shapeID) {
    //move center point
    siganlNoise = Math.abs(mouseX - centerX);
    //siganlPower = Math.abs(mouseY - centerY);
    if ((ind)%30==1)
        //console.log("siganlNoise- "+siganlNoise);
    deltaRmssd  = 0;
    deltaRmssd = Math.min((exteranlRmssd)*2,200);

    siganlNoise = deltaRmssd; 

   // if ((ind++)%30==1)
   //     console.log("deltaRmssd- "+deltaRmssd);



    // create springing effect
    siganlNoise *= springing;

    
    accelX += siganlNoise;
    accelY += siganlNoise;
    // move predator's center
    //centerX += accelX;
    //centerY += accelY;
    // slow down springing
    accelX *= damping;
    accelY *= damping;
    // change curve tightness
    organicConstant = 1 - ((abs(accelX) + abs(accelY)) * 0.1);
    //move nodes
    for (var i = 0; i < nodes; i++) {
        nodeX[shapeID][i] = nodeStartX[shapeID][i] + sin(radians(angle[shapeID][i])) * (accelX * 2);
        nodeY[shapeID][i] = nodeStartY[shapeID][i] + sin(radians(angle[shapeID][i])) * (accelY * 2);
        angle[shapeID][i] += frequency[shapeID][i];
    }
}
