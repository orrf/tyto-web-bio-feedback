function getrmssd_Tomer(arr)
{

    let sum = 0;
    let interval1 = 0;
    let interval2 = 0;
    
    for (let index = 0; index < arr.length-2; index++) {
        
        interval1 = (arr[index+1]-arr[index])*1000/60;
        interval2 = (arr[index+2]-arr[index+1])*1000/60;
    
        sum += (interval1-interval2) ** 2;
        
    }
    
    let rmssd = Math.sqrt((sum)/(arr.length-1));


    return(rmssd);
}



function sum(a) {
    return a.reduce((acc, val) => acc + val)
}


function stddev(arr) {
    const arr_mean = mean(arr)
    const r = function(acc, val) {
        return acc + ((val - arr_mean) * (val - arr_mean))
    }
    return Math.sqrt(arr.reduce(r, 0.0) / arr.length)
}

function average(array) {
    return array.reduce((a, b) => a + b) / array.length;
}

function meanNNI(intervals) {
    return average(intervals) 
}

function arrayDiff(array){
    let arrayDiff = []
    for (let i = 1; i < array.length; i++) {
        arrayDiff.push(array[i] - array[i-1])
    }
    return arrayDiff
}

function std(intervals) {
    let distFromMeanSquared = []
    let intervalAverage = average(arrayDiff(intervals))
    intervals.forEach(element => {
        distFromMeanSquared.push((element - intervalAverage) ** 2)
    });
    return average(distFromMeanSquared)
}

function rmssd_alex (intervals) {
    let aquaredArrayDiff = [] 
    arrayDiff(intervals).forEach(element => {
        aquaredArrayDiff.push(element ** 2)    
    });
    return average(aquaredArrayDiff)**0.5
}


function getIndexesOfPeaksFromPeakArray(arr) {
    var indexes = [], i;
    for(i = 0; i < arr.length; i++)
        if (arr[i] === 1)
            indexes.push(i);
    return indexes;
}

function buildPeakTimesArr(signals, timestampArr){
    var output = []
    signals.forEach(element => {
        output.push(timestampArr[element])
    });
    return output
}
function getRmssdAlex(signals, timestampArr){
    return rmssd_alex(arrayDiff(buildPeakTimesArr(signals, timestampArr)))
}